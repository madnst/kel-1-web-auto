<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>list_kota</name>
   <tag></tag>
   <elementGuidId>861b3575-6c6f-49dc-a114-d2082debf1a9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'kota' and (text() = 'Pilih kotaBandungBogorJemberKediriLumajangMalangPasuruanProbolinggoYogyakarta' or . = 'Pilih kotaBandungBogorJemberKediriLumajangMalangPasuruanProbolinggoYogyakarta')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='kota']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#kota</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>33d3b442-69e4-4f8c-97ee-ef93adad7a09</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control</value>
      <webElementGuid>c0d45900-46b4-4354-9ab7-0565e3ee2cb8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>kota</value>
      <webElementGuid>e0494325-f0de-4664-b41e-5cf14b9686e1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Pilih kotaBandungBogorJemberKediriLumajangMalangPasuruanProbolinggoYogyakarta</value>
      <webElementGuid>85a3d380-0c6d-4d8b-b1de-7a16cf339e42</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;kota&quot;)</value>
      <webElementGuid>a934f210-e210-428e-8e93-791f5ebda0de</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='kota']</value>
      <webElementGuid>2d4c38a8-6de9-4cfe-9b1e-765f6f5e6c9c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div/div[2]/form/div[2]/select</value>
      <webElementGuid>e38b9ca8-14bf-443d-9353-7b8979d3cf99</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kota*'])[1]/following::select[1]</value>
      <webElementGuid>fc82a801-f747-4330-a780-228869f47ab0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Nama*'])[1]/following::select[1]</value>
      <webElementGuid>4c69c161-1f1b-4de5-a560-c62836f2cc18</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Alamat*'])[1]/preceding::select[1]</value>
      <webElementGuid>e1d83493-1cae-45d6-8e19-f981b130a5c8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='No.Handphone*'])[1]/preceding::select[1]</value>
      <webElementGuid>5719b313-7b74-4bcd-9aa0-e70d03726cc7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//select</value>
      <webElementGuid>acf4ceeb-7c50-40dc-a751-cbc2bca8b5af</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@id = 'kota' and (text() = 'Pilih kotaBandungBogorJemberKediriLumajangMalangPasuruanProbolinggoYogyakarta' or . = 'Pilih kotaBandungBogorJemberKediriLumajangMalangPasuruanProbolinggoYogyakarta')]</value>
      <webElementGuid>d1244c57-afd5-419c-882a-ff60497ebf3c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
