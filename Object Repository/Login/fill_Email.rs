<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>fill_Email</name>
   <tag></tag>
   <elementGuidId>688bca74-64ee-44d8-8719-df302a6f570e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='exampleInputEmail1']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#exampleInputEmail1</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>663fcd36-1972-40a0-aea3-d60f90ac57ef</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>email</value>
      <webElementGuid>c7a12f21-4a01-4f20-9c08-b1ddb1bab2d8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control</value>
      <webElementGuid>bb302a0c-0301-480f-8334-56ffff36a80a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>exampleInputEmail1</value>
      <webElementGuid>7a73fc78-261e-41b8-a3c3-ad854b2b5852</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Contoh: johndee@gmail.com</value>
      <webElementGuid>c101cc6d-33cd-47aa-97dd-54bc9e140a38</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>hellotremi@gmail.com</value>
      <webElementGuid>f9629f66-6e92-48be-8875-0f51b42c3a56</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;exampleInputEmail1&quot;)</value>
      <webElementGuid>75cffed5-615c-4bc6-b26e-e335a4a3d33b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='exampleInputEmail1']</value>
      <webElementGuid>23bb789e-5f96-4bcd-976c-405affbcc05c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div/div[2]/form/div/input</value>
      <webElementGuid>16b4d29c-5bc6-4adb-9137-3d957ee35fc9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//input</value>
      <webElementGuid>9380e98c-8866-4af5-ba67-b961743883f7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'email' and @id = 'exampleInputEmail1' and @placeholder = 'Contoh: johndee@gmail.com']</value>
      <webElementGuid>48a4b86e-d576-43e8-8bcb-3139b1b96e27</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
