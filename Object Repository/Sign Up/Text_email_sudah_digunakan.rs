<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Text_email_sudah_digunakan</name>
   <tag></tag>
   <elementGuidId>1342ec3c-61ef-45ec-9ac2-aad65991db7e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/div/div[2]/form/div/strong</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.alert.alert-danger.alert-dismissible.fade.show > strong</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>strong</value>
      <webElementGuid>75fdbdaf-eb97-4e50-a02f-2fc8924582d4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Email sudah digunakan</value>
      <webElementGuid>a4984860-a18d-415d-ac8e-062c632f5af2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;App&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-sm-6 form-register&quot;]/form[1]/div[@class=&quot;alert alert-danger alert-dismissible fade show&quot;]/strong[1]</value>
      <webElementGuid>58954273-fabc-496b-adfe-d3b3022f3972</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div/div[2]/form/div/strong</value>
      <webElementGuid>aec8aea9-50d3-48e0-989b-3ee352c0b393</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Daftar'])[1]/following::strong[1]</value>
      <webElementGuid>6d009c5a-c1be-48dd-ad50-b99d3ae98e0c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SecondHand.'])[1]/following::strong[2]</value>
      <webElementGuid>135daf0b-6bf8-46a4-ac38-cfc5fbe537eb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Nama'])[1]/preceding::strong[1]</value>
      <webElementGuid>cd04b40e-a916-4a30-a43d-3c0055756029</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Email address'])[1]/preceding::strong[1]</value>
      <webElementGuid>72d5c46a-ed47-44fb-bde1-6fcfad2bc6f5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Email sudah digunakan']/parent::*</value>
      <webElementGuid>c3a283ae-d092-4cd2-9042-20c6b4f14a99</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/strong</value>
      <webElementGuid>5cea5956-da54-4316-a523-3604bd04a5eb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//strong[(text() = 'Email sudah digunakan' or . = 'Email sudah digunakan')]</value>
      <webElementGuid>e4ae5423-1eef-4bd2-a5d8-d2b0110847e9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
