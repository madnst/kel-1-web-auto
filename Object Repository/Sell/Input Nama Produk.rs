<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Input Nama Produk</name>
   <tag></tag>
   <elementGuidId>bc3e9a81-b24c-4973-9ec2-265225581bdf</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='nm_produk']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#nm_produk</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>70228120-ea05-4e09-912e-61c7be049f51</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>type</value>
      <webElementGuid>b4297db9-00c0-4030-831d-49169065163e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control</value>
      <webElementGuid>e86f7555-6273-4db6-b6a1-c7138e4038e3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>nm_produk</value>
      <webElementGuid>85a78f52-7fde-4e16-897c-16f7c91a140f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Nama Produk</value>
      <webElementGuid>84d1a175-5629-4558-8562-b470ec9cf6b6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;nm_produk&quot;)</value>
      <webElementGuid>aa6c0e87-85f8-4566-b5f1-fae22a9dbd66</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='nm_produk']</value>
      <webElementGuid>b451b959-0c09-4159-86ee-4f5a47cafb26</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div/div/form/div/input</value>
      <webElementGuid>f0fd518d-4ace-4553-b70d-799da47373b4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//input</value>
      <webElementGuid>fa61f104-31c2-404f-974e-e658eaebc005</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'type' and @id = 'nm_produk' and @placeholder = 'Nama Produk']</value>
      <webElementGuid>1befa94d-1e21-47e7-94b2-6e9a414d90fd</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
