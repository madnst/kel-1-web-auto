<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Card Barang</name>
   <tag></tag>
   <elementGuidId>8de9e8eb-f3e1-4063-a1ca-3f3f1980ce99</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'card-img-top' and @alt = 'Product Jar']</value>
      </entry>
      <entry>
         <key>XPATH</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>card-img-top</value>
      <webElementGuid>60ab1499-bc2f-4e81-ad39-6cf770d0130a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>alt</name>
      <type>Main</type>
      <value>Product Jar</value>
      <webElementGuid>b513a653-51e2-4c2c-86f5-88b7b8d4be8d</webElementGuid>
   </webElementProperties>
</WebElementEntity>
