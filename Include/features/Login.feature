@Login
Feature: Login
	As a user, I want to login in Secondhand web

	@TC_LO001
	Scenario: TC_LO001 - User want to login using unverified email
		Then User click button masuk disini
		Then User input unverified_email "zabyt@getnada.com"
		Then user input new_password "12345"
		Then User click button masuk_ke_web

	@TC_LO002
	Scenario: TC_LO002 - User want to login using incorrect credential
		Then User input incorrect_email "weowe@getnada.com"
		Then User input incorrect_password "12345"
		Then User click button masuk_ke_web

	@TC_LO003
	Scenario: TC_LO003 - User want to login using correct credential
		Then User input email_seller "medycy@getnada.com"
		Then User input password_seller "Satu2345"
		Then User click button masuk_ke_web
