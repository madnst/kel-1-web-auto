@Sell
Feature: Sell
  As a user, I want to sell a product

  @TC_SE001
  Scenario: TC_SE001_Post Product Without data
    Then User click button jual
    Then User click button terbitkan
    Then User wait the delay_satu

  @TC_SE002
  Scenario: TC_SE002_Post Product Only Name Data
    Then User click button home
    Then User click button jual
    Then User input nama produk "Mobil"
    Then User click button terbitkan
    Then User wait the delay_dua

  @TC_SE003
  Scenario: TC_SE003_Post Product All Data
    Then User click button home
    Then User click button jual
    Then User input nama produk "Mobil"
    Then User input harga produk "200000000"
    Then User choose kategori "Hoby"
    Then User input deskripsi "Mobil Keren"
    Then User click button terbitkan
    Then User wait the delay_tiga
