@Signup
Feature: Signup
  As a user, I want to Signup in Secondhand web

  @TC_SU001
  Scenario: TC_SU001 - User want to sign up with the existing credential
  	Then User click button masuk
  	Then User click signup in here
    Then User input name "Tremi"
    Then User input with the existing email "medycy@getnada.com"
    Then User input password "12345Satu"
    Then User click on signup button
    Then User wait the delay_satu

 	@TC_SU002
  Scenario: TC_SU002 - User want to sign up new credential using special character in username
    Then User input special character in username "tremi@3/"
    Then User input email "jugol@robot-mail.com"
    Then User input password "12345"
    Then User click on signup button
    Then User wait the delay_satu
    
  @TC_SU003
  Scenario: TC_SU003 - User want to sign up new credential
    Then User input name "tremi"
    Then User input new email "jugol@robot-mail.com"
    Then User input password "Satu2345"
    Then User click on button signup
    Then User wait the delay_satu