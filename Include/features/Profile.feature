@profile
Feature: Profile
  
  As a user, I want to update my profile in website

  @TC_PR001
  Scenario: TC_PR001_Submit Profile
    Then User click Logo Profile
    Then User click Tulisan Profile
    Then User input Nama_User "Imaduddin"
    Then User choose Kota_User "Bandung"
    Then User input Alamat_User "Bumi"
    Then User input Nomor_HP_User "0821211212121"
    Then User click Button Submit_Profile
		Then User wait the delay_satu
		Then User click button home