@search
Feature: Search
  As a user, I want to filter product

  @TC_SA001
  Scenario: TC_SA_001_User filter kategori Baju
  	Then User scroll down
    Then User click kategori_baju
    Then User wait the delay_satu

  @TC_SA002
  Scenario: TC_SA_002_User filter kategori Elektronik
    Then User click kategori_elektronik
    Then User wait the delay_satu

  @TC_SA003
  Scenario: TC_SA_003_User filter kategori Hoby
    Then User click kategori_hoby
    Then User wait the delay_satu

  @TC_SA004
  Scenario: TC_SA_001_User filter kategori Kendaraan
    Then User click kategori_kendaraan
    Then User wait the delay_satu

  @TC_SA005
  Scenario: TC_SA_001_User filter kategori Kesehatan
    Then User click kategori_kesehatan
    Then User wait the delay_satu
