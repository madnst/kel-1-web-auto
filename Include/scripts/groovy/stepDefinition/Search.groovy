package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class Search {
	@Then("User click kategori_baju")
	public void user_click_kategori_baju() {
		WebUI.click(findTestObject('Search/button_Baju'))
	}

	@Then("User click kategori_elektronik")
	public void user_click_kategori_elektronik() {
		WebUI.click(findTestObject('Search/button_Elektronik'))
	}

	@Then("User click kategori_hoby")
	public void user_click_kategori_hoby() {
		WebUI.click(findTestObject('Search/button_Hoby'))
	}

	@Then("User click kategori_kendaraan")
	public void user_click_kategori_kendaraan() {
		WebUI.click(findTestObject('Search/button_Kendaraan'))
	}

	@Then("User click kategori_kesehatan")
	public void user_click_kategori_kesehatan() {
		WebUI.click(findTestObject('Search/button_Kesehatan'))
	}

	@Then("User scroll down")
	public void user_scroll_down() {
		WebUI.scrollToPosition(0, 250)
	}
}