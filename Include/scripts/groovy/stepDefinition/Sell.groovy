package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class Sell {
	@Then("User click button masuk")
	public void user_click_button_masuk() {
		WebUI.click(findTestObject('Login/btn_Masuk1'))
	}

	@Then("User input email_seller {string}")
	public void user_input_email_seller(String email_seller) {
		WebUI.setText(findTestObject('Login/fill_Email'), 'medycy@getnada.com')
	}

	@Then("User input password_seller {string}")
	public void user_input_password_seller(String password_seller) {
		WebUI.setText(findTestObject('Login/fill_Password'), 'Satu2345')
	}

	@Then("User click button masuk_ke_web")
	public void user_click_button_masuk_ke_web() {
		WebUI.click(findTestObject('Login/btn_Masuk2'))
	}

	@Then("User wait the delay_satu")
	public void user_wait_the_delay_satu() {
		WebUI.delay(5)
	}

	@Then("User wait the delay_dua")
	public void user_wait_the_delay_dua() {
		WebUI.delay(5)
	}

	@Then("User wait the delay_tiga")
	public void user_wait_the_delay_tiga() {
		WebUI.delay(5)
	}

	@Then("User click button jual")
	public void user_click_button_jual() {
		WebUI.click(findTestObject('Sell/Tombol Jual'))
	}

	@Then("User click button terbitkan")
	public void user_click_button_terbitkan() {
		WebUI.click(findTestObject('Sell/Tombol Terbitkan'))
	}

	@Then("User click button home")
	public void user_click_button_home() {
		WebUI.click(findTestObject('Sell/Tombol Home'))
	}

	@Then("User input nama produk {string}")
	public void user_input_nama_produk(String nama) {
		WebUI.setText(findTestObject('Sell/Input Nama Produk'), 'Mobil')
	}

	@Then("User input harga produk {string}")
	public void user_input_harga_produk(String harga) {
		WebUI.setText(findTestObject('Sell/Input Harga Produk'), '200000000')
	}

	@Then("User choose kategori {string}")
	public void user_choose_kategori(String kategori) {
		WebUI.selectOptionByValue(findTestObject('Sell/Pilih kategori'), '1', false)
	}

	@Then("User input deskripsi {string}")
	public void user_input_deskripsi(String deskripsi) {
		WebUI.setText(findTestObject('Sell/Deskripsi'), 'Mobil Keren')
	}
}