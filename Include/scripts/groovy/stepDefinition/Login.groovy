package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class Login {
	@Then("User input unverified_email {string}")
	public void user_input_unverified_email(String unverified_email) {
		WebUI.setText(findTestObject('Login/fill_Email'), 'zabyt@getnada.com')
	}

	@Then("user input new_password {string}")
	public void user_input_new_password(String new_password) {
		WebUI.setText(findTestObject('Login/fill_Password'), '12345')
	}

	@Then("User input incorrect_email {string}")
	public void user_input_incorrect_email(String incorrect_email) {
		WebUI.setText(findTestObject('Login/fill_Email'), 'weowe@getnada.com')
	}

	@Then("User input incorrect_password {string}")
	public void user_input_incorrect_password(String incorrect_password) {
		WebUI.setText(findTestObject('Login/fill_Password'), '12345')
	}

	@Then("User click button masuk disini")
	public void user_click_button_masuk_disini() {
		WebUI.click(findTestObject('Login/a_Masuk di sini'))
	}
}