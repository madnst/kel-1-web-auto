package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class profile {
	@Then("User click Logo Profile")
	public void user_click_Logo_Profile() {
		WebUI.click(findTestObject('Profile/Logo Profile'))
	}

	@Then("User click Tulisan Profile")
	public void user_click_Tulisan_Profile() {
		WebUI.click(findTestObject('Profile/Tulisan Profile'))
	}

	@Then("User input Nama_User {string}")
	public void user_input_Nama_User(String Nama_User) {
		WebUI.setText(findTestObject('Profile/field_nama'), 'Imaduddin')
	}

	@Then("User choose Kota_User {string}")
	public void user_choose_Kota_User(String Kota_User) {
		WebUI.selectOptionByValue(findTestObject('Profile/list_kota'), 'Bandung', false)
	}

	@Then("User input Alamat_User {string}")
	public void user_input_Alamat_User(String Alamat_User) {
		WebUI.setText(findTestObject('Profile/field_alamat'), 'Bumi')
	}

	@Then("User input Nomor_HP_User {string}")
	public void user_input_Nomor_HP_User(String Nomor_HP_User) {
		WebUI.setText(findTestObject('Profile/field_nohape'), '0821211212121')
	}

	@Then("User click Button Submit_Profile")
	public void user_click_Button_Submit_Profile() {
		WebUI.click(findTestObject('Profile/button_submitprofle'))
	}
}