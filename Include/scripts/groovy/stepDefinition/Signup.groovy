package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class Signup {
	@Then("User click button login")
	public void user_click_button_login() {
		WebUI.click(findTestObject('Sign Up/btn_Masuk'))
	}

	@Then("User click signup in here")
	public void user_click_signup_in_here() {
		WebUI.click(findTestObject('Sign Up/btn_Daftar_disini'))
	}

	@Then("User input name {string}")
	public void user_input_name(String Username) {
		WebUI.setText(findTestObject('Sign Up/fill_Name'), 'tremi')
	}

	@Then("User input with the existing email {string}")
	public void user_input_with_the_existing_email(String email) {
		WebUI.setText(findTestObject('Sign Up/fill_Email'), 'medycy@getnada.com')
	}

	@Then("User input password {string}")
	public void user_input_password(String password) {
		WebUI.setText(findTestObject('Sign Up/fill_Password'), '12345Satu')
	}

	@Then("User click button signup")
	public void user_click_button_signup() {
		WebUI.click(findTestObject('Sign Up/btn_Daftar'))
	}

	@Then("User input special character in username {string}")
	public void user_input_special_character_in_username(String username) {
		WebUI.setText(findTestObject('Sign Up/fill_Name'), 'tremi@3/')
	}

	@Then("User input email {string}")
	public void user_input_email(String email) {
		WebUI.setText(findTestObject('Sign Up/fill_Email'), 'jugol@robot-mail.com')
	}

	@Then("User click on signup button")
	public void user_click_on_signup_button() {
		WebUI.click(findTestObject('Sign Up/btn_Daftar'))
	}

	@Then("User input new email {string}")
	public void user_input_new_email(String email) {
		WebUI.setText(findTestObject('Sign Up/fill_Email'), 'jugol@robot-mail.com')
	}

	@Then("User click on button signup")
	public void user_click_on_button_signup() {
		WebUI.click(findTestObject('Sign Up/btn_Daftar'))
	}
}
