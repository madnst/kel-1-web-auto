import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('https://secondhand-store.herokuapp.com/')

WebUI.click(findTestObject('Sign Up/btn_Masuk'))

WebUI.click(findTestObject('Sign Up/btn_Daftar_disini'))

WebUI.setText(findTestObject('Sign Up/fill_Name'), 'tremi')

WebUI.setText(findTestObject('Sign Up/fill_Email'), 'medycy@getnada.com')

WebUI.setText(findTestObject('Sign Up/fill_Password'), 'Satu2345')

WebUI.click(findTestObject('Sign Up/btn_Daftar'))

WebUI.verifyElementVisible(findTestObject('Sign Up/Text_email_sudah_digunakan'))

