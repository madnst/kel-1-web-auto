import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import internal.GlobalVariable as GlobalVariable

WebUI.click(findTestObject('Login/btn_Masuk1'))

WebUI.setText(findTestObject('Login/fill_Email'), 'medycy@getnada.com')

WebUI.setText(findTestObject('Login/fill_Password'), 'Satu2345')

WebUI.click(findTestObject('Login/btn_Masuk2'))

WebUI.click(findTestObject('Buy/Card Barang'))

WebUI.scrollToPosition(0, 5)

WebUI.click(findTestObject('Buy/btn_Saya tertarik dan ingin nego'))

WebUI.setText(findTestObject('Buy/field_Harga Tawar'), '10000')

WebUI.click(findTestObject('Buy/btn_Kirim'))

WebUI.delay(5)

